terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.52.0"
    }
  }

  backend "gcs" {
    bucket = "terraform_state-1"
    prefix = "learning_gke"
  }

  required_version = ">= 0.14"
}
